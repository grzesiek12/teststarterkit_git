# Switching between the branches #

> you have to switch to another branch

## Context ##
Switching between branches is needed when:

- You are working on more than one branches (tasks) at the same time
- You have to review/test code from the colleague branch

## Steps ##
1. commit/discard your actual changes
2. switch between the branches
3. make sure that you have moved well and you are up to date in this branch

###### Assumptions: ######
- I am on random branch
- Branch that I want to switch exists

### Console ###
1) To check your actual status of changes, type:
```
git status
```

2) You have to decide if uncommitted changes should be committed or discarded:

- to commit, check [commit and push](./commit_and_push.md)
- to discard, type:
```
git reset --hard
```

3) To switch branch, type:
```
git checkout feature/feature2
```

4) To make sure that you are in good branch, type:
```
git status
```

After successful switch you should see similar output:
```
$ git status
On branch feature/feature2
nothing to commit, working tree clean
```

### SourceTree ###